package com.epam.exception;

@SuppressWarnings("serial")
public class AssociateException extends RuntimeException {
	public AssociateException(String msg) {
		super(msg);
	}
}
