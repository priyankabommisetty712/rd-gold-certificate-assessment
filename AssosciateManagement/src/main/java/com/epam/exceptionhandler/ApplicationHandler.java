package com.epam.exceptionhandler;

import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.epam.exception.AssociateException;

import lombok.extern.slf4j.Slf4j;


@RestControllerAdvice
@Slf4j
public class ApplicationHandler {
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ExceptionResponse handleMethodArgumentsNotValid(MethodArgumentNotValidException e,WebRequest request) {
		log.info("Encountered MethodArgumentNotValidException");
		List<String> errors=e.getAllErrors().stream().map(error->error.getDefaultMessage()).toList();
		return new ExceptionResponse(new Date().toString(), 
				HttpStatus.BAD_REQUEST.toString(), 
				errors.toString(), 
				request.getDescription(false));
		
	}
	
	
	@ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
	public ExceptionResponse handleHttpMessageNotReadableException(HttpMessageNotReadableException e,WebRequest request) {
		log.info("Encountered HttpMessageNotReadableException");
		return new ExceptionResponse(new Date().toString(), 
				HttpStatus.BAD_REQUEST.toString(), 
				"invalid Json format",
				request.getDescription(false));

	}

	@ExceptionHandler(AssociateException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ExceptionResponse handleBatchException(AssociateException e,WebRequest request) {	
		log.info("Encountered AssociateException");
		return new ExceptionResponse(new Date().toString(), 
				HttpStatus.NOT_FOUND.toString(), 
				e.getMessage(),
				request.getDescription(false));

	}
	
	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ExceptionResponse handleRuntimeException(RuntimeException e,WebRequest request) {	
		log.info("Encountered RuntimeException");
		return new ExceptionResponse(new Date().toString(), 
				HttpStatus.INTERNAL_SERVER_ERROR.toString(), 
				e.getMessage(),
				request.getDescription(false));

	}
	

}
