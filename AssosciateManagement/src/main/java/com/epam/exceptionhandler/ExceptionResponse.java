package com.epam.exceptionhandler;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ExceptionResponse {
	String timestamp;
	String status;
	String error;
	String path;
	
}

