package com.epam.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.dtos.AssociateDto;
import com.epam.entities.Associate;
import com.epam.exception.AssociateException;
import com.epam.repository.AssociateRepo;
import com.epam.repository.BatchRepo;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AssociateServiceImpl implements AssociateService {
	@Autowired
	private AssociateRepo associateRepo;
	@Autowired
	private BatchRepo batchRepo;
	@Autowired
	private ModelMapper modelMapper;

	@Override
	public AssociateDto addAssociate(AssociateDto associateDto) {
		log.info("Entered addAssociate() in service");
		Associate associate=modelMapper.map(associateDto, Associate.class);
		Associate existAssociate= batchRepo.findById(associateDto.getBatch().getId()).map(batch->{
			associate.setBatch(batch);
			return associate;
		}).orElse(associate);
		Associate savedAssociate=associateRepo.save(existAssociate);
		log.info("saved successfully");
		return modelMapper.map(savedAssociate,AssociateDto.class);
	}

	@Override
	public List<AssociateDto> getAssociatesByGender(String gender) {
		log.info("Entered getAssociatesByGender() in service");
		return associateRepo.findAllByGender(gender).stream().map(associate->modelMapper.map(associate, AssociateDto.class)).toList();
		
	}

	@Override
	public AssociateDto modifyAssociate(AssociateDto associateDto) {
		log.info("Entered modifyAssociate() in service");
		if(associateRepo.existsById(associateDto.getId())) {
			Associate updatedAssociate= associateRepo.save(modelMapper.map(associateDto, Associate.class));
			log.info("Modified successfully");
			return modelMapper.map(updatedAssociate, AssociateDto.class);
		}
		else {
			throw new AssociateException("Associate doesnot exists with such Id");
		}
	}

	@Override
	public void deleteAssociate(int id) {
		log.info("Entered deleteAssociate() in service");
		associateRepo.deleteById(id);
		
	}

}
