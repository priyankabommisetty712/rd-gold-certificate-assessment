package com.epam.service;

import java.util.List;

import com.epam.dtos.AssociateDto;

public interface AssociateService {
	AssociateDto addAssociate(AssociateDto associateDto);
	List<AssociateDto> getAssociatesByGender(String gender);
	AssociateDto modifyAssociate(AssociateDto associateDto);
	void deleteAssociate(int id);
}
