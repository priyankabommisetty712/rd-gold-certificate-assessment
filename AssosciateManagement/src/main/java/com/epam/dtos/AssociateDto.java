package com.epam.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class AssociateDto {
	@Schema(accessMode=AccessMode.READ_ONLY)
	private int id;
	@NotBlank(message = "associate name should be present")
	private String name;
	@Email(message = "Email should be in proper format")
	private String email;
	@NotBlank(message="gender should be either male or female")
	private String gender;
	@NotBlank(message="college name should be present")
	private String college;
	@NotBlank(message="status should be either active or inactive")
	private String status;
	@Valid
	private BatchDto batch;
	
}
